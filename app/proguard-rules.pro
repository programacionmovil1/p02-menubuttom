
# Preserve the names of classes used in XML layouts
-keepclassmembers class * {
    @androidx.annotation.Keep *;
}

-keepclassmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

# Preserve the name of the class members used in reflection
-keepclassmembers class * {
    ** ** (android.content.Context, android.util.AttributeSet);
    ** ** (android.content.Context, android.util.AttributeSet, int);
    ** ** (android.content.Context, android.util.AttributeSet, int, int);
}

# Preserve the names of custom views and their attributes
-keepclassmembers class * extends android.view.View {
    public <init>(android.content.Context);
    public <init>(android.content.Context, android.util.AttributeSet);
    public <init>(android.content.Context, android.util.AttributeSet, int);
}

# Preserve all annotations
-keepattributes *Annotation*

# Preserve line number information for debugging stack traces
-keepattributes SourceFile,LineNumberTable

# Preserve the names of methods and fields annotated with @Keep
-keepclassmembers class * {
    @androidx.annotation.Keep <fields>;
    @androidx.annotation.Keep <methods>;
}

# Preserve classes that implement Parcelable
-keepclassmembers class * implements android.os.Parcelable {
    static ** CREATOR;
}

# Preserve the names of the Parcelable implementations
-keepclassmembers class * implements android.os.Parcelable {
    public static final android.os.Parcelable$Creator *;
}

# Preserve the names of methods annotated with @OnClick
-keepclassmembers class * {
    @android.view.View$OnClickListener *;
}

# Picasso - Image loading library
-keep class com.squareup.picasso.** { *; }
-dontwarn com.squareup.picasso.**

# Avoid warnings for missing annotations
-dontwarn javax.annotation.**

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# If you keep the line number information, uncomment this to
# hide the original source file name.
-renamesourcefileattribute SourceFile
