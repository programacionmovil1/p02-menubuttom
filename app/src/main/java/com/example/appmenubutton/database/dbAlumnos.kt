package com.example.appmenubutton.database

import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.widget.Toast
import androidx.core.content.contentValuesOf

class dbAlumnos(private val context: Context) {
    private val dbHelper: AlumnosDbHelper = AlumnosDbHelper(context)
    private lateinit var db: SQLiteDatabase

    private val leerCampos = arrayOf(
        DefinirTabla.Alumnos.ID,
        DefinirTabla.Alumnos.MATRICULA,
        DefinirTabla.Alumnos.NOMBRE,
        DefinirTabla.Alumnos.DOMICILIO,
        DefinirTabla.Alumnos.ESPECIALIDAD,
        DefinirTabla.Alumnos.FOTO
    )

    fun openDataBase() {
        try {
            db = dbHelper.writableDatabase
        } catch (e: Exception) {
            Toast.makeText(context, "Error al abrir la base de datos", Toast.LENGTH_SHORT).show()
        }
    }

    fun InsertarAlumno(alumno: Alumno): Long {
        return try {
            val valores = contentValuesOf().apply {
                put(DefinirTabla.Alumnos.MATRICULA, alumno.matricula)
                put(DefinirTabla.Alumnos.NOMBRE, alumno.nombre)
                put(DefinirTabla.Alumnos.DOMICILIO, alumno.domicilio)
                put(DefinirTabla.Alumnos.ESPECIALIDAD, alumno.especialidad)
                put(DefinirTabla.Alumnos.FOTO, alumno.foto)
            }
            db.insert(DefinirTabla.Alumnos.TABLA, null, valores)
        } catch (e: Exception) {
            Toast.makeText(context, "Error al insertar alumno", Toast.LENGTH_SHORT).show()
            -1L
        }
    }

    fun ActualizarAlumno(alumno: Alumno, id: Int): Int {
        return try {
            val valores = contentValuesOf().apply {
                put(DefinirTabla.Alumnos.MATRICULA, alumno.matricula)
                put(DefinirTabla.Alumnos.NOMBRE, alumno.nombre)
                put(DefinirTabla.Alumnos.DOMICILIO, alumno.domicilio)
                put(DefinirTabla.Alumnos.ESPECIALIDAD, alumno.especialidad)
                put(DefinirTabla.Alumnos.FOTO, alumno.foto)
            }
            db.update(DefinirTabla.Alumnos.TABLA, valores, "${DefinirTabla.Alumnos.ID} = ?", arrayOf(id.toString()))
        } catch (e: Exception) {
            Toast.makeText(context, "Error al actualizar alumno", Toast.LENGTH_SHORT).show()
            0
        }
    }

    fun BorrarAlumno(matricula: String): Int {
        return try {
            db.delete(DefinirTabla.Alumnos.TABLA, "${DefinirTabla.Alumnos.MATRICULA} = ?", arrayOf(matricula))
        } catch (e: Exception) {
            Toast.makeText(context, "Error al borrar alumno", Toast.LENGTH_SHORT).show()
            0
        }
    }

    fun getAlumno(matricula: String): Alumno {
        return try {
            val db = dbHelper.readableDatabase
            val cursor = db.query(
                DefinirTabla.Alumnos.TABLA, leerCampos,
                "${DefinirTabla.Alumnos.MATRICULA} = ?", arrayOf(matricula),
                null, null, null
            )
            if (cursor.moveToFirst()) {
                mostrarAlumnos(cursor)
            } else {
                Alumno()
            }
        } catch (e: Exception) {
            Toast.makeText(context, "Error al obtener alumno", Toast.LENGTH_SHORT).show()
            Alumno()
        }
    }

    fun getAllAlumnos(): List<Alumno> {
        return try {
            val db = dbHelper.readableDatabase
            val cursor = db.query(
                DefinirTabla.Alumnos.TABLA, leerCampos,
                null, null, null, null, null
            )
            val alumnos = mutableListOf<Alumno>()
            while (cursor.moveToNext()) {
                alumnos.add(mostrarAlumnos(cursor))
            }
            cursor.close()
            alumnos
        } catch (e: Exception) {
            Toast.makeText(context, "Error al obtener la lista de alumnos", Toast.LENGTH_SHORT).show()
            emptyList()
        }
    }

    private fun mostrarAlumnos(cursor: Cursor): Alumno {
        return Alumno().apply {
            id = cursor.getInt(0)
            matricula = cursor.getString(1)
            nombre = cursor.getString(2)
            domicilio = cursor.getString(3)
            especialidad = cursor.getString(4)
            foto = cursor.getString(5)
        }
    }

    fun close() {
        try {
            dbHelper.close()
        } catch (e: Exception) {
            Toast.makeText(context, "Error al cerrar la base de datos", Toast.LENGTH_SHORT).show()
        }
    }
}
