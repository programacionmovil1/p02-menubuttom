package com.example.appmenubutton

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import android.widget.Toast

class ListFragment : Fragment() {
    private lateinit var listView: ListView
    private lateinit var searchView: SearchView
    private lateinit var arrayList: ArrayList<String>
    private lateinit var adapter: ArrayAdapter<String>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_list, container, false)
        listView = view.findViewById(R.id.lstAlumnos)
        searchView = view.findViewById(R.id.searchview)

        val items = resources.getStringArray(R.array.alumnos)

        arrayList = ArrayList()
        arrayList.addAll(items)

        adapter = ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, arrayList)
        listView.adapter = adapter

        listView.onItemClickListener = AdapterView.OnItemClickListener { parent, view, position, id ->
            val alumno: String = parent.getItemAtPosition(position).toString()
            val builder = AlertDialog.Builder(requireContext())
            builder.setTitle("Lista de Alumnos")
            builder.setMessage("$position : $alumno")
            builder.setPositiveButton("Ok") { dialog, which ->
                // Handle the "Ok" button click here
            }
            builder.show()
        }

        // Configurar el SearchView para filtrar la lista
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                // Remover espacios antes de filtrar
                val filteredText = newText?.replace("\\s+".toRegex(), "") ?: ""
                try {
                    adapter.filter.filter(filteredText)
                } catch (e: Exception) {
                    Toast.makeText(requireContext(), "Error al filtrar la lista", Toast.LENGTH_SHORT).show()
                }
                return false
            }
        })

        return view
    }
}
