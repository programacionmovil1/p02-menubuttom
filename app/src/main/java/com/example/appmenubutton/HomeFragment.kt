package com.example.appmenubutton

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast

class HomeFragment : Fragment() {

    @SuppressLint("MissingInflatedId")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        // Configurar datos del alumno
        val studentImage: ImageView = view.findViewById(R.id.student_image)
        val studentName: TextView = view.findViewById(R.id.student_name)
        val subject: TextView = view.findViewById(R.id.subject)
        val course: TextView = view.findViewById(R.id.course)
        val gitlabIcon: ImageView = view.findViewById(R.id.gitlab_icon)
        val gmailIcon: ImageView = view.findViewById(R.id.gmail_icon)
        val uni: TextView = view.findViewById(R.id.Uni)

        // Configurar la imagen del alumno
        try {
            studentImage.setImageResource(R.drawable.student_image) // Reemplaza con tu recurso de imagen
        } catch (e: Exception) {
            Toast.makeText(requireContext(), "Error al cargar la imagen del estudiante", Toast.LENGTH_SHORT).show()
        }

        // Configurar textos del alumno
        studentName.text = "Axel Jovani Ruiz Guerrero"
        subject.text = "Programacion Movil"
        course.text = "Ingeniería de Sistemas"
        uni.text = "Universidad Politecnica de Sinaloa"

        // Configurar clics en los iconos de redes sociales
        gitlabIcon.setOnClickListener {
            val url = "https://gitlab.com/jovaniruiz117"
            abrirPagina(url)
        }

        gmailIcon.setOnClickListener {
            val email = "mailto:jovanircontacto@gmail.com"
            abrirPagina(email)
        }

        return view
    }

    private fun abrirPagina(url: String) {
        try {
            val webpage: Uri = Uri.parse(url)
            val intent = Intent(Intent.ACTION_VIEW, webpage)
            if (intent.resolveActivity(requireContext().packageManager) != null) {
                startActivity(intent)
            } else {
                Toast.makeText(requireContext(), "No se encontró una aplicación para abrir este enlace", Toast.LENGTH_SHORT).show()
            }
        } catch (e: Exception) {
            Toast.makeText(requireContext(), "Error al abrir el enlace", Toast.LENGTH_SHORT).show()
        }
    }
}
