package com.example.appmenubutton

import com.example.appmenubutton.database.Alumno

interface AlumnoListener {
    fun onAlumnoAdded(alumno: Alumno)
}
