package com.example.appmenubutton

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.fragment.app.Fragment
import com.example.appmenubutton.database.Alumno
import com.google.android.material.bottomnavigation.BottomNavigationView
import android.widget.Toast
import androidx.activity.enableEdgeToEdge

class MainActivity : AppCompatActivity(), AlumnoListener {
    private lateinit var bottomNavigationView: BottomNavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)

        bottomNavigationView = findViewById(R.id.btnNavegador)
        bottomNavigationView.setOnItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.btnhome -> {
                    cambiarFrame(HomeFragment())
                    true
                }
                R.id.btnLista -> {
                    cambiarFrame(ListFragment())
                    true
                }
                R.id.btnDB -> {
                    cambiarFrame(dbFragment())
                    true
                }
                R.id.btnSalir -> {
                    cambiarFrame(AcercaFragment())
                    true
                }
                else -> false
            }
        }

        // Cargar el HomeFragment por defecto
        if (savedInstanceState == null) {
            cambiarFrame(HomeFragment())
            bottomNavigationView.selectedItemId = R.id.btnhome
        }

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        // Solicitar permisos necesarios
        checkAndRequestPermissions()
    }

    private fun cambiarFrame(fragment: Fragment) {
        try {
            supportFragmentManager.beginTransaction().replace(R.id.frmContenedor, fragment).commit()
        } catch (e: Exception) {
            Toast.makeText(this, "Error al cambiar el fragmento", Toast.LENGTH_SHORT).show()
        }
    }

    private fun checkAndRequestPermissions() {
        val permissions = arrayOf(
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
        )
        val permissionsToRequest = permissions.filter {
            ContextCompat.checkSelfPermission(this, it) != PackageManager.PERMISSION_GRANTED
        }
        if (permissionsToRequest.isNotEmpty()) {
            requestPermissionsLauncher.launch(permissionsToRequest.toTypedArray())
        }
    }

    private val requestPermissionsLauncher =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
            permissions.entries.forEach {
                if (!it.value) {
                    Toast.makeText(this, "Permiso ${it.key} denegado. La aplicación puede no funcionar correctamente.", Toast.LENGTH_SHORT).show()
                }
            }
        }

    override fun onAlumnoAdded(alumno: Alumno) {
        val fragment = supportFragmentManager.findFragmentById(R.id.frmContenedor)
        if (fragment is AcercaFragment) {
            fragment.onAlumnoAdded(alumno)
        }
    }
}
